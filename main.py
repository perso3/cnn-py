from CONST import *
from functions import *
from PIL import Image
import numpy
import glob


def analyse_img():
    global img, newImagePixels, y, x, modelImg
    img = loadImage('images/jul.jpg')
    imgBand = imageData2D(img)
    imgBand = normData(imgBand)
    print(imgBand[0])
    print(imageConv(imgBand)[0])
    conv = imgBand
    dataList = []
    for pat in PATTERNS:
        conv = imgBand
        while len(conv) > 16:
            conv = imageConv(conv, pat)
            conv = imagePool(conv)
        dataList.append(conv)
        # print('\n'.join(' '.join(str(round(x)) for x in row) for row in conv))
        # print('\n')
    modelList = []
    for i, data in enumerate(dataList):
        newImagePixels = numpy.zeros((len(data[0]), len(data), 3), dtype=numpy.uint8)

        model = []
        for y in range(0, len(data)):
            row = []
            for x in range(0, len(data[y])):
                newImagePixels[y][x] = grayscaleToRGB(data[y][x])
                if round(data[y][x]) == 0:
                    row.append(" ")
                else:
                    row.append("■")
            model.append(row)
        modelImg = Image.fromarray(newImagePixels)
        modelImg = modelImg.resize((len(data[0]) * 50, len(data) * 50), Image.AFFINE)
        modelImg.save(f'generated_images/model{i + 1}.jpg')
        modelList.append(model)
    # for model in modelList:
    #     print('\n'.join(' '.join(x for x in row) for row in model))
    #     print('\n')

    data = modelSum(dataList)
    newImagePixels = numpy.zeros((len(data[0]), len(data), 3), dtype=numpy.uint8)
    model = []
    for y in range(0, len(data)):
        row = []
        for x in range(0, len(data[y])):
            newImagePixels[y][x] = grayscaleToRGB(data[y][x])
            if round(data[y][x]) == 0:
                row.append(" ")
            else:
                row.append("■")
        model.append(row)
    modelImg = Image.fromarray(newImagePixels)
    modelImg = modelImg.resize((len(data[0]) * 50, len(data) * 50), Image.AFFINE)
    modelImg.save(f'generated_images/model_total.jpg')
    print('\n'.join(' '.join(str(x) for x in row) for row in model))


if __name__ == '__main__':
    analyse_img()
