from vector import Vector


class Img:
    """
    A class that contains yes
    
    ...

    Attributes
    ----------
    

    Methods
    -------
    
    
    """

    def __init__(self, x, y, color):
        """Constructor for Image"""
        self.position = Vector(x, y)
        self.color = color

    def getColor(self):
        """Getter for color from Img class"""
        return self.color

    def getPos(self):
        """Getter for position from Img class"""
        return self.position

    def __str__(self) -> str:
        """__str__ for Image"""
        return ''
