from PIL import Image, ImageOps


def loadImage(link):
    img = Image.open(link, 'r')
    img = ImageOps.grayscale(img)
    return img


def grayscaleToRGB(value):
    return [value * 255, value * 255, value * 255]


def imageData2D(image):
    imgBand = list(image.getdata())
    imgSize = image.size
    return [[imgBand[j] for j in range(i * imgSize[0], i * imgSize[0] + imgSize[0])] for i in range(0, imgSize[1])]


def normData(data):
    return [[round(data[i][j] / 255, 4) for j in range(0, len(data[i]))] for i in range(0, len(data))]


def imageConv(data, pattern=None, padding=False):
    res = []
    if pattern is None:
        pattern = [[0, 0, 1],
                   [0, 0, 1],
                   [0, 0, 0]]

    if padding:
        pass
    else:
        for i in range(1, len(data) - 1):
            temp = []
            for j in range(1, len(data[i]) - 1):
                pixel = [[data[i + y - 1][j + x - 1] * pattern[y][x] for x in range(0, len(pattern[y]))] for y in
                         range(0, len(pattern))]
                # temp.append(sum(list(map(sum, pixel)))/9)
                temp.append(max(pixel))

            res.append(temp)

    return res


def imagePool(data):
    res = []
    for i in range(0, len(data) - 1, 2):
        row = []
        for j in range(0, len(data[i]) - 1, 2):
            temp = [data[i][j], data[i][j + 1], data[i + 1][j], data[i + 1][j + 1]]
            row.append(max(min(temp)))

        res.append(row)

    return res

def modelSum(models):
    temp = models[0]
    res = [[0 for x in range(len(temp[0]))] for y in range(len(temp))]
    for model in models:
        for y, y_val in enumerate(model):
            for x, x_val in enumerate(y_val):
                res[y][x] += x_val

    for y, y_val in enumerate(res):
        for x, x_val in enumerate(y_val):
            res[y][x] = x_val / len(temp[y])

    return res