import keyboard
import pyautogui
from PIL import Image

#(674, 324)


def click(x,y):
    pyautogui.click(x,y)

def pressKey(keys):
    pyautogui.press(keys)

def writePixel(start , x, y):
    posX = int(start[0]) + x
    posY = int(start[1]) + y
    click(posX,posY)

def linePixels(start, startLine, endLine):
    slx = startLine.x +start[0]
    sly = startLine.y +start[1]

    elx = endLine.x +start[0]
    ely = endLine.y +start[1]

    pyautogui.moveTo(slx,sly)
    pyautogui.dragTo(elx,ely)


def interupt():
    return keyPressed('esc')

def keyPressed(key):
    return keyboard.is_pressed(key)

def selectColor(col):

    black = (1014, 79)
    white = (1014, 101)

    if col == 1:
        click(white[0], white[1])
    else:
        click(black[0], black[1])
